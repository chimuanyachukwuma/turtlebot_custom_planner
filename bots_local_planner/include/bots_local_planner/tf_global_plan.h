#ifndef TF_GLOBAL_PLAN_
#define TF_GLOBAL_PLAN_

#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>
#include <nav_msgs/Odometry.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Point.h>
#include <tf/transform_listener.h>

#include <string>
#include <cmath>

#include <angles/angles.h>
#include <costmap_2d/costmap_2d.h>

namespace bots_local_planner
{

    bool getInterimGoalPose(const std::vector<geometry_msgs::PoseStamped> &global_plan,
                         double look_ahead,
                         geometry_msgs::PoseStamped &interim_goal_pose);

    bool getTransformedPose(geometry_msgs::PoseStamped &interim_goal_pose,
                            tf::Stamped<tf::Pose> &goal_pose);
};

#endif