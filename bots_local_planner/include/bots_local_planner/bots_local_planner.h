#ifndef BOTS_LOCAL_PLANNER_H_
#define BOTS_LOCAL_PLANNER_H_

#include <ros/ros.h>

#include <base_local_planner/odometry_helper_ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <tf/transform_listener.h>
#include <nav_msgs/Odometry.h>
#include <nav_msgs/Path.h>
#include <std_msgs/Bool.h>

#include <dynamic_reconfigure/server.h>

#include <costmap_2d/costmap_2d.h>
#include <costmap_2d/costmap_2d_ros.h>
#include <nav_core/base_local_planner.h>
#include "bots_local_planner/planner_utils.h"
#include "bots_local_planner/tf_global_plan.h"
#include "bots_local_planner/BotsLocalPlannerConfig.h"

#include <iostream>
#include <memory>
#include <string>
#include <vector>

namespace bots_local_planner
{

    class BotsLocalPlanner : public nav_core::BaseLocalPlanner
    {
    public:
        BotsLocalPlanner();

        ~BotsLocalPlanner();

        bool computeVelocityCommands(geometry_msgs::Twist &cmd_vel);

        void initialize(std::string name, tf2_ros::Buffer *tf, costmap_2d::Costmap2DROS *costmap_ros);

        bool isGoalReached();

        bool setPlan(const std::vector<geometry_msgs::PoseStamped> &global_plan);

    private:
        std::unique_ptr<LocalPlannerUtils> local_planner_utils;
        double goal_x, goal_y, goal_yaw;
        double final_goal_x, final_goal_y, final_goal_yaw;
        bool initialized_, goal_reached_;
        bool heading_reached_, distance_covered_;
        std::string global_frame_, base_frame_;
        bots_local_planner::BotsLocalPlannerConfig cfg_;
        tf2_ros::Buffer *tf_;
        std::vector<geometry_msgs::PoseStamped> global_plan_;
        tf::Stamped<tf::Pose> goal_pose;
        ros::Publisher local_plan_pub;
        costmap_2d::Costmap2DROS *costmap_ros_; ///< @brief The ROS wrapper for the costmap the controller will use
        costmap_2d::Costmap2D *costmap_;        ///< @brief The costmap the controller will use
        // Publishers
        ros::Publisher target_pose_pub, current_pose_pub;
        // Subscribers
        ros::Subscriber emergency_stop_sub;
        // Planner config parameters
        double dt_;
        double look_ahead_;
        double max_linear_vel_, min_linear_vel_, max_incr_vel_;
        double max_angle_vel_, min_angle_vel_, min_incr_vel_;
        double kp_linear_, ki_linear_, kd_linear_;
        double kp_angle_, ki_angle_, kd_angle_;
        double position_precision_, orientation_precision_;
        double linear_error_, angle_error_;
        double integral_linear_, integral_angle_;

        // Odometry
        base_local_planner::OdometryHelperRos *odom_helper_;

        // Dynamic reconfigure server
        dynamic_reconfigure::Server<BotsLocalPlannerConfig> *dynamic_srv;
        void reconfigureCB(BotsLocalPlannerConfig &config, uint32_t level);

        void initializePlannerUtils();
    };
};

#endif