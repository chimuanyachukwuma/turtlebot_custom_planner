#ifndef PLANNER_UTILS_H_
#define PLANNER_UTILS_H_

#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>
#include <nav_msgs/Odometry.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Point.h>
#include <tf/transform_listener.h>

#include <string>
#include <cmath>

#include <angles/angles.h>
#include <costmap_2d/costmap_2d.h>
#include <costmap_2d/costmap_2d_ros.h>

class LocalPlannerUtils
{
public:
    LocalPlannerUtils(double dt,
                      double kp_linear,
                      double ki_linear,
                      double kd_linear,
                      double kp_angle,
                      double ki_angle,
                      double kd_angle,
                      double min_incr_vel,
                      double max_incr_vel,
                      double min_linear_vel,
                      double max_linear_vel,
                      double min_angle_vel,
                      double max_angle_vel);

    ~LocalPlannerUtils();

    void getOdomPose(tf::Stamped<tf::Pose> goal_pose,
                     double &position_x,
                     double &position_y,
                     double &yaw);

    std::vector<double> getCurrentRobotPose(costmap_2d::Costmap2DROS *costmap_ros);

    double computePIDAngularVelocity(nav_msgs::Odometry &base_odometry,
                                     double goal_yaw,
                                     double curr_yaw,
                                     double angle_error_,
                                     double integral_angle_);

    double computePIDLinearVelocity(nav_msgs::Odometry &base_odometry,
                                    double goal_x,
                                    double goal_y,
                                    double linear_error_,
                                    double integral_linear_);

    bool checkPathHeadingAchieved(double goal_yaw,
                                  double curr_yaw,
                                  double orientation_precision);

    double getGoalDistance(double curr_x,
                           double curr_y,
                           double goal_x,
                           double goal_y);

private:
    double dt_;
    double kp_linear_;
    double ki_linear_;
    double kd_linear_;
    double linear_error_;
    double min_incr_vel_;
    double max_incr_vel_;
    double min_linear_vel_;
    double max_linear_vel_;
    double integral_linear_;
    double kp_angle_;
    double ki_angle_;
    double kd_angle_;
    double angle_error_;
    double min_angle_vel_;
    double max_angle_vel_;
    double integral_angle_;
};

#endif