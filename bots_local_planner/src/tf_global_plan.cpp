#include <bots_local_planner/tf_global_plan.h>

namespace bots_local_planner
{
    bool getInterimGoalPose(const std::vector<geometry_msgs::PoseStamped> &global_plan,
                            double look_ahead,
                            geometry_msgs::PoseStamped &interim_goal_pose)
    {
        if (global_plan.empty())
        {
            ROS_ERROR("Received plan with zero length");
            return false;
        }

        interim_goal_pose = global_plan[0]; // get any random pose

        if (look_ahead < global_plan.size() - 1)
        {
            interim_goal_pose.pose.position.x = global_plan[look_ahead].pose.position.x;
            interim_goal_pose.pose.position.y = global_plan[look_ahead].pose.position.y;
            interim_goal_pose.pose.orientation = global_plan[look_ahead].pose.orientation;
        }
        else
        {
            interim_goal_pose.pose.position.x = global_plan[global_plan.size() - 1].pose.position.x;
            interim_goal_pose.pose.position.y = global_plan[global_plan.size() - 1].pose.position.y;
            interim_goal_pose.pose.orientation = global_plan[global_plan.size() - 1].pose.orientation;
        }

        return true;
    }

    bool getTransformedPose(geometry_msgs::PoseStamped &interim_goal_pose,
                            tf::Stamped<tf::Pose> &goal_pose)
    {

        std::string global_frame_ = interim_goal_pose.header.frame_id;
        std::string base_frame_ = "base_footprint";

        try
        {
            tf::StampedTransform transform;
            tf::TransformListener listener;

            listener.waitForTransform(base_frame_,
                                global_frame_,
                                ros::Time::now(),
                                ros::Duration(4.0));

            listener.lookupTransform(base_frame_,
                                global_frame_,
                                ros::Time(0),
                                transform);

            tf::poseStampedMsgToTF(interim_goal_pose, goal_pose);
            goal_pose.setData(transform * goal_pose);
            goal_pose.stamp_ = transform.stamp_;
            goal_pose.frame_id_ = base_frame_;

            // *x = ps.pose.position.x;
            // *y = ps.pose.position.y;
            // *theta = tf::getYaw(ps.pose.orientation);
        }
        catch (tf::LookupException &ex)
        {
            ROS_ERROR("No Transform available Error: %s\n", ex.what());
            ros::Duration(1.0).sleep();
            return false;
        }
        catch (tf::ConnectivityException &ex)
        {
            ROS_ERROR("Connectivity Error: %s\n", ex.what());
            ros::Duration(1.0).sleep();
            return false;
        }
        catch (tf::ExtrapolationException &ex)
        {
            ROS_ERROR("Extrapolation Error: %s\n", ex.what());
            ros::Duration(1.0).sleep();

            return false;
        }
        return true;
    }
}