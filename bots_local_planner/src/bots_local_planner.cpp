#include <pluginlib/class_list_macros.h>
#include "bots_local_planner/bots_local_planner.h"

using namespace std;

// register planner plugin
PLUGINLIB_EXPORT_CLASS(bots_local_planner::BotsLocalPlanner, nav_core::BaseLocalPlanner)

namespace bots_local_planner
{

  BotsLocalPlanner::BotsLocalPlanner()
      : initialized_(false), goal_reached_(false), heading_reached_(false), distance_covered_(false)
  {}

  BotsLocalPlanner::~BotsLocalPlanner()
  {
    if (dynamic_srv)
    {
      delete dynamic_srv;
    }
  }

  bool BotsLocalPlanner::computeVelocityCommands(geometry_msgs::Twist &cmd_vel)
  {
    if (!initialized_)
    {
      ROS_ERROR("PID planner has not been initialized.");
      return false;
    }
    if (goal_reached_)
    {
      ROS_ERROR("BotsLocalPlanner already at goal position.");
      return true;
    }

    // Get interim pose and transform wrt base_link
    geometry_msgs::PoseStamped interim_goal_pose;
    tf::Stamped<tf::Pose> goal_pose;
    final_goal_x = global_plan_[global_plan_.size() - 1].pose.position.x;
    final_goal_y = global_plan_[global_plan_.size() - 1].pose.position.x;
    final_goal_yaw = tf::getYaw(global_plan_[global_plan_.size() - 1].pose.orientation);

    // Get next goal pose from global plan
    bots_local_planner::getInterimGoalPose(global_plan_, look_ahead_, interim_goal_pose);
    bots_local_planner::getTransformedPose(interim_goal_pose, goal_pose);
    local_planner_utils->getOdomPose(goal_pose, goal_x, goal_y, goal_yaw);

    // Get current robot pose
    std::vector<double> robot_pose = local_planner_utils->getCurrentRobotPose(costmap_ros_);
    double curr_x = robot_pose[0];
    double curr_y = robot_pose[1];
    double curr_yaw = robot_pose[2];

    std::cout << "------------------- Computing velocity" << std::endl;
    ROS_INFO("------------------Goal X: %f, Y: %f, Yaw: %f", goal_x, goal_y, goal_yaw);
    ROS_INFO("------------------Current X: %f, Y: %f, Yaw: %f", curr_x, curr_y, curr_yaw);

    // Get robot velocities in robot frame using Odom
    nav_msgs::Odometry base_odometry;
    odom_helper_->getOdom(base_odometry);
 
    // is orientation for next path reached. If it is, move robot in x velocity, otherwise move in angular fashion
    if (local_planner_utils->checkPathHeadingAchieved(goal_yaw, curr_yaw, angle_error_))
    {
      // heading_reached_ = true;
      //  if heading is correct, and distance is covered
      if (local_planner_utils->getGoalDistance(curr_x, curr_y, goal_x, goal_y) <= position_precision_)
      {
        // distance_covered_ = true;
      }
      else
      {
        double vel_x = local_planner_utils->computePIDLinearVelocity(base_odometry, goal_x, goal_y, angle_error_, integral_angle_);
        cmd_vel.linear.x = vel_x;
        cmd_vel.linear.y = 0.0;
        cmd_vel.angular.z = 0.0;
      }
    }
    else
    {
      double theta_vel = local_planner_utils->computePIDAngularVelocity(base_odometry, goal_yaw, curr_yaw, linear_error_, integral_linear_);
      cmd_vel.linear.x = 0.0;
      cmd_vel.linear.y = 0.0;
      cmd_vel.angular.z = theta_vel;
    }

    // Take final orientation into account; rotate in place
    bool final_distance = (local_planner_utils->getGoalDistance(curr_x, curr_y, final_goal_x, final_goal_y) <= position_precision_);
    bool final_heading = local_planner_utils->checkPathHeadingAchieved(final_goal_yaw, curr_yaw, angle_error_);

    if (final_distance && final_heading)
    {
      goal_reached_ = true;
    }

    return true;
  }

  void BotsLocalPlanner::initialize(std::string name, tf2_ros::Buffer *tf, costmap_2d::Costmap2DROS *costmap_ros)
  {
    if (!initialized_)
    {
      // create Node Handle with name of plugin
      ros::NodeHandle nh = ros::NodeHandle("~/" + name);
      tf_ = tf;
      // Initialize base_local_planner odom and robot poses
      local_plan_pub = nh.advertise<nav_msgs::Path>("local_plan", 1);
      target_pose_pub = nh.advertise<geometry_msgs::PoseStamped>("/target_pose", 10);
      current_pose_pub = nh.advertise<geometry_msgs::PoseStamped>("/current_pose", 10);

      // set dynamic reconfigure callback
      dynamic_srv = new dynamic_reconfigure::Server<BotsLocalPlannerConfig>(nh);
      dynamic_reconfigure::Server<BotsLocalPlannerConfig>::CallbackType cb =
          boost::bind(&BotsLocalPlanner::reconfigureCB, this, _1, _2);
      dynamic_srv->setCallback(cb);

      initializePlannerUtils();

      initialized_ = true;
      goal_reached_ = false;

      // initialize the copy of the costmap the controller will use
      costmap_ros_ = costmap_ros;
      costmap_ = costmap_ros_->getCostmap();

      global_frame_ = costmap_ros_->getGlobalFrameID();
      base_frame_ = costmap_ros_->getBaseFrameID();

      // Set time interval, dt
      double controller_frequency;
      nh.param("/move_base/controller_frequency", controller_frequency, 20.0);
      dt_ = 1 / controller_frequency;

      ROS_INFO("BotsLocalPlanner has been initialized!");
    }
    else
    {
      ROS_WARN("BotsLocalPlanner has already been initialized.");
    }
  }

  bool BotsLocalPlanner::isGoalReached()
  {
    if (!initialized_)
    {
      ROS_ERROR("BotsLocalPlanner has not been initialized.");
      return false;
    }
    if (goal_reached_)
    {
      ROS_ERROR("Goal has been reached.");
      return true;
    }

    return goal_reached_;
  }

  bool BotsLocalPlanner::setPlan(const std::vector<geometry_msgs::PoseStamped> &global_plan)
  {
    if (!initialized_)
    {
      ROS_ERROR("BotsLocalPlanner has not been initialized.");
      return false;
    }
    // set new plan
    global_plan_.clear();
    global_plan_ = global_plan;

    // reset plan parameters
    goal_reached_ = false;
    distance_covered_ = false;
    heading_reached_ = false;

    // reset PID parameters
    linear_error_ = angle_error_ = 0.0;
    integral_linear_ = integral_angle_ = 0.0;

    std::cout << "------------------- Setting Plan" << std::endl;

    return true;
  }

  void BotsLocalPlanner::reconfigureCB(BotsLocalPlannerConfig &config, uint32_t level)
  {
    // goal tolerance
    position_precision_ = config.position_precision;
    orientation_precision_ = config.orientation_precision;
    // linear
    max_linear_vel_ = config.max_linear_vel;
    min_linear_vel_ = config.min_linear_vel;
    max_incr_vel_ = config.max_incr_vel;
    // angular
    max_angle_vel_ = config.max_angle_vel;
    min_angle_vel_ = config.min_angle_vel;
    min_incr_vel_ = config.min_incr_vel;
    // pid controller params
    kp_linear_ = config.kp_linear;
    ki_linear_ = config.ki_linear;
    kd_linear_ = config.kd_linear;
    kp_angle_ = config.kp_angle;
    ki_angle_ = config.ki_angle;
    kd_angle_ = config.kd_angle;

    look_ahead_ = config.look_ahead;

    std::cout << "------------------- Dynamic Reconfiguring" << std::endl;
  }

  void BotsLocalPlanner::initializePlannerUtils()
  {
    local_planner_utils.reset(new LocalPlannerUtils(dt_,
                                          kp_linear_,
                                          ki_linear_,
                                          kd_linear_,
                                          kp_angle_,
                                          ki_angle_,
                                          kd_angle_,
                                          min_incr_vel_,
                                          max_incr_vel_,
                                          min_linear_vel_,
                                          max_linear_vel_,
                                          min_angle_vel_,
                                          max_angle_vel_));
  }
}