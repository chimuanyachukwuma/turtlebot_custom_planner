#include "base_local_planner.h"
#include <costmap_2d/costmap_2d_ros.h>
#include <pluginlib/class_list_macros.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <base_local_planner/goal_functions.h>
#include "bots_local_planner/BotsLocalPlannerConfig.h"

using namespace std;

// register planner plugin
PLUGINLIB_EXPORT_CLASS(bots_local_planner::BotsLocalPlanner, nav_core::BaseLocalPlanner)

namespace base_local_planner
{

  BotsLocalPlanner::BotsLocalPlanner()
      : initialized_(false), goal_reached_(false) {}

  BotsLocalPlanner::~BotsLocalPlanner()
  {
    if (dynamic_srv)
    {
      delete dynamic_srv;
    }
  }

  bool BotsLocalPlanner::computeVelocityCommands(geometry_msgs::Twist &cmd_vel)
  {
    if (!initialized_)
    {
      ROS_ERROR("PID planner has not been initialized.");
      return false;
    }
    if (goal_reached_)
    {
      ROS_ERROR("BotsLocalPlanner already at goal position.");
      return true;
    }
    // next target
    geometry_msgs::PoseStamped target;
    geometry_msgs::PoseStamped current_pose;

    double t_x, t_y, t_th;
    double x_vel = 0, th_vel = 0;
    double t_th_w = 0.0;

    // looking for the next point in the path far enough with minimum difference in angle
    while (plan_index_ < global_plan_.size())
    {
      target = global_plan_[plan_index_];
      // finding next point in path to get the orientation
      int next_plan_index = min(((int)global_plan_.size()) - 1, plan_index_ + 1);
      // getting target orientation in world coord based on next target and the
      // one after it
      t_th_w = atan2((global_plan_[next_plan_index].pose.position.y -
                      global_plan_[plan_index_].pose.position.y),
                     (global_plan_[next_plan_index].pose.position.x -
                      global_plan_[plan_index_].pose.position.x));
      // edit target adding orientation from t_th_w
      tf::Quaternion th_target_quat = tf::createQuaternionFromYaw(t_th_w);
      target.pose.orientation.x = th_target_quat[0];
      target.pose.orientation.y = th_target_quat[1];
      target.pose.orientation.z = th_target_quat[2];
      target.pose.orientation.w = th_target_quat[3];
      getTransformedPosition(target, &t_x, &t_y, &t_th);

      if (hypot(t_x, t_y) > p_window_ || fabs(t_th) > o_window_)
      {
        break;
      }
      plan_index_++;
      // cout << global_plan_.size() << " : " << next_plan_index << " -> " << plan_index_ << endl;
    }

    if (plan_index_ >= global_plan_.size() - 1)
    {
      getTransformedPosition(global_plan_.back(), &t_x, &t_y, &t_th);
    }

    // invoking robot pose and orientaiton
    tf::Stamped<tf::Pose> global_pose;
    costmap_ros_->getRobotPose(global_pose);
    robot_current_pose = global_pose.getOrigin();
    robot_current_orientation = tf::getYaw(global_pose.getRotation());
    //********************************************************************************
    // odometry observation - getting robot velocities in robot frame
    nav_msgs::Odometry base_odom;
    odom_helper_->getOdom(base_odom);

    // get final goal orientation - Quaternion to Euler
    final_orientation = getEulerAngles(global_plan_.back());
    //********************************************************************************
    if (getGoalPositionDistance(global_plan_.back(), robot_current_pose[0], robot_current_pose[1]) <= position_precision_)
    {
      // check to see if the goal orientation has been reached
      if (fabs(final_orientation[2] - robot_current_orientation) <= orientation_precision_)
      {
        // set the velocity command to zero
        cmd_vel.linear.x = 0.0;
        cmd_vel.linear.y = 0.0;
        cmd_vel.angular.z = 0.0;
        rotating_to_goal_ = false;
        goal_reached_ = true;
        cout << "pose and orientation are reached..." << endl;
        // isGoalReached();
        // return true;
      }
      else
      {
        th_vel = AngularPIDController(base_odom, final_orientation[2], robot_current_orientation);
        cmd_vel.linear.x = 0;
        cmd_vel.linear.y = 0;
        cmd_vel.angular.z = th_vel;
        cout << "need to rotate on place... " << endl;
      }
    }
    else
    { // quide the robot to the final goal position in the path
      // if (fabs(t_th_w - robot_current_orientation) <= orientation_precision_*2) {
      //********Linear velocity controller-PID*************
      x_vel = LinearPIDController(base_odom, t_x, t_y);
      //}
      //********Angular velocity controller-PID************
      if (plan_index_ >= global_plan_.size() - 5)
      {
        t_th_w = final_orientation[2];
      }
      th_vel = AngularPIDController(base_odom, t_th_w, robot_current_orientation);
      //********************************************************************************
      cmd_vel.linear.x = x_vel;
      cmd_vel.linear.y = 0;
      cmd_vel.angular.z = th_vel;
    }

    // publish next target pose
    target.header.frame_id = "/map";
    target.header.stamp = ros::Time::now();
    target_pose_pub_.publish(target);

    // publish robot pose
    curr_pose.header.frame_id = "/map";
    curr_pose.header.stamp = ros::Time::now();
    tf::Quaternion curr_orien_quat =
        tf::createQuaternionFromYaw(robot_current_orientation);
    curr_pose.pose.position.x = robot_current_pose[0];
    curr_pose.pose.position.y = robot_current_pose[1];
    curr_pose.pose.position.z = robot_current_pose[2];
    curr_pose.pose.orientation.x = curr_orien_quat[0];
    curr_pose.pose.orientation.y = curr_orien_quat[1];
    curr_pose.pose.orientation.z = curr_orien_quat[2];
    curr_pose.pose.orientation.w = curr_orien_quat[3];
    current_pose_pub.publish(curr_pose);

    return true;
  }

  void BotsLocalPlanner::initialize(std::string name, tf::TransformListener *tf, costmap_2d::Costmap2DROS *costmap_ros)
  {
    if (!initialized_)
    {
      // create Node Handle with name of plugin
      ros::NodeHandle nh = ros::NodeHandle("~/" + name);
      tf_ = tf;
      // Initialize base_local_planner odom and robot poses
      odom_helper_ = new base_local_planner::OdometryHelperRos("/odom");
      target_pose_pub_ = nh.advertise<geometry_msgs::PoseStamped>("/target_pose", 10);
      current_pose_pub = nh.advertise<geometry_msgs::PoseStamped>("/current_pose", 10);

      // set dynamic reconfigure callback
      dynamic_srv_ = new dynamic_reconfigure::Server<BotsLocalPlannerConfig>(nh);
      dynamic_reconfigure::Server<BotsLocalPlannerConfig>::CallbackType dynamicServerCallback =
          boost::bind(&BotsLocalPlanner::reconfigureCB, this, _1, _2);
      dynamic_srv_->setCallback(dynamicServerCallback);

      robot_current_pose[0] = 0;
      robot_current_pose[1] = 0;
      robot_current_pose[2] = 0;
      robot_current_orientation = 0;

      initialized_ = true;
      goal_reached_ = false;

      plan_index_ = 0;
      last_plan_index_ = 0;

      // initialize the copy of the costmap the controller will use
      costmap_ros_ = costmap_ros;
      costmap_ = costmap_ros_->getCostmap();

      global_frame_ = costmap_ros_->getGlobalFrameID();
      base_frame_ = costmap_ros_->getBaseFrameID();

      // Set time interval, dt
      double controller_frequency;
      nh.param("/move_base/controller_frequency", controller_frequency, 20.0);
      dt_= 1 / controller_frequency;

      ROS_INFO("BotsLocalPlanner has been initialized!");
    }
    else
    {
      ROS_WARN("BotsLocalPlanner has already been initialized.");
    }
  }

  bool BotsLocalPlanner::isGoalReached()
  {
    if (!initialized_)
    {
      ROS_ERROR("BotsLocalPlanner has not been initialized.");
      return false;
    }
    if (goal_reached_)
    {
      ROS_ERROR("Goal has been reached.");
      return true;
    }
    if (plan_index_ >= global_plan_.size() - 1)
    {
      // last pose
      if (fabs(final_orientation[2] - robot_current_orientation) <= orientation_precision_ &&
          getGoalPositionDistance(global_plan_.back(), robot_current_pose[0], robot_current_pose[1]) <= position_precision_)
      {
        goal_reached_ = true;
        robotStops();
        ROS_INFO("Goal has been reached!");
      }
    }
    return goal_reached_;
  }

  bool BotsLocalPlanner::setPlan(const std::vector<geometry_msgs::PoseStamped> &global_plan)
  {
    if (!initialized_)
    {
      ROS_ERROR("BotsLocalPlanner has not been initialized.");
      return false;
    }
    // set new plan
    global_plan_.clear();
    global_plan_ = global_plan;
    // reset plan parameters
    goal_reached_ = false;
    plan_index_ = 0;
    // reset PID parameters
    integral_linearear_ = integral_anglele_ = 0.0;
    linear_errorear_ = angle_errorle_ = 0.0;
    return true;
  }

  void BotsLocalPlanner::publishPlan(const std::vector<geometry_msgs::PoseStamped> &path, const ros::Publisher &pub)
  {
    // given an empty path we won't do anything
    if (path.empty())
      return;

    // create a path message
    nav_msgs::Path gui_path;
    gui_path.poses.resize(path.size());
    gui_path.header.frame_id = path[0].header.frame_id;
    gui_path.header.stamp = path[0].header.stamp;

    // Extract the plan in world co-ordinates, we assume the path is all in the
    // same frame
    for (unsigned int i = 0; i < path.size(); i++)
    {
      gui_path.poses[i] = path[i];
    }

    pub.publish(gui_path);
  }

  double BotsLocalPlanner::getGoalPositionDistance(const geometry_msgs::PoseStamped &global_pose, double goal_x, double goal_y)
  {
    return hypot(goal_x - global_pose.pose.position.x,
                 goal_y - global_pose.pose.position.y);
  }

  double BotsLocalPlanner::getGoalOrientationAngleDifference(const geometry_msgs::PoseStamped &global_pose, double t_th_w)
  {
    tf::Quaternion q(
        global_pose.pose.orientation.x, global_pose.pose.orientation.y,
        global_pose.pose.orientation.z, global_pose.pose.orientation.w);
    tf::Matrix3x3 m(q);
    double roll, pitch, yaw;
    m.getRPY(roll, pitch, yaw);
    double angle = t_th_w - yaw;
    // cout << angle  << "  "  << t_th_w << "  "  << yaw << endl;
    double a = fmod(fmod(angle, 2.0 * M_PI) + 2.0 * M_PI, 2.0 * M_PI);
    if (a > M_PI)
    {
      a -= 2.0 * M_PI;
    }
    return a;
  }

  std::vector<double> BotsLocalPlanner::getEulerAngles(geometry_msgs::PoseStamped &Pose)
  {
    std::vector<double> EulerAngles;
    EulerAngles.resize(3, 0);
    tf::Quaternion q(Pose.pose.orientation.x, Pose.pose.orientation.y,
                     Pose.pose.orientation.z, Pose.pose.orientation.w);
    tf::Matrix3x3 m(q);
    m.getRPY(EulerAngles[0], EulerAngles[1], EulerAngles[2]);
    return EulerAngles;
  }

  double BotsLocalPlanner::LinearPIDController(nav_msgs::Odometry &base_odometry, double next_t_x, double next_t_y)
  {
    double vel_curr = hypot(base_odometry.twist.twist.linear.y, base_odometry.twist.twist.linear.x);

    double vel_target = hypot(next_t_x, next_t_y) / dt_; // linear velocity of depends on the p_windows (direct relation)

    if (fabs(vel_target) > max_linear_vel_)
    {
      vel_target = copysign(max_linear_vel_, vel_target);
    }

    double err_vel = vel_target - vel_curr;

    integral_linearear_ += err_vel * dt_;
    double derivative_lin = (err_vel - linear_errorear_) / dt_;
    double incr_lin = kp_linear_ * err_vel + ki_linear_ * integral_linearear_ + kd_linear_ * derivative_lin;
    linear_errorear_ = err_vel;

    if (fabs(incr_lin) > max_incr_vel_)
      incr_lin = copysign(max_incr_vel_, incr_lin);

    double x_velocity = vel_curr + incr_lin;

    if (fabs(x_velocity) > max_linear_vel_)
      x_velocity = copysign(max_linear_vel_, x_velocity);
    if (fabs(x_velocity) < min_linear_vel_)
      x_velocity = copysign(min_linear_vel_, x_velocity);

    return x_velocity;
  }

  double BotsLocalPlanner::AngularPIDController(nav_msgs::Odometry &base_odometry, double target_th_w, double robot_orien)
  {
    double orien_err = target_th_w - robot_orien;
    if (orien_err > M_PI)
      orien_err -= (2 * M_PI);
    if (orien_err < -M_PI)
      orien_err += (2 * M_PI);
    double target_vel_ang = (orien_err) / dt_;
    if (fabs(target_vel_ang) > max_angle_vel)
    {
      target_vel_ang = copysign(max_angle_vel, target_vel_ang);
    }

    double vel_ang = base_odometry.twist.twist.angular.z;
    double angle_error = target_vel_ang - vel_ang;
    integral_anglele_ += angle_error * dt_;
    double derivative_ang = (angle_error - angle_errorle_) / dt_;
    double incr_ang = kp_angle_ * angle_error + ki_angle_ * integral_anglele_ +
                      kd_angle_ * derivative_ang;
    angle_errorle_ = angle_error;

    if (fabs(incr_ang) > min_incr_vel)
      incr_ang = copysign(min_incr_vel, incr_ang);

    double th_velocity = copysign(vel_ang + incr_ang, target_vel_ang);
    if (fabs(th_velocity) > max_angle_vel)
      th_velocity = copysign(max_angle_vel, th_velocity);
    if (fabs(th_velocity) < min_angle_vel)
      th_velocity = copysign(min_angle_vel, th_velocity);

    return th_velocity;
  }

  void BotsLocalPlanner::getTransformedPosition(geometry_msgs::PoseStamped &pose, double *x, double *y, double *theta)
  {
    geometry_msgs::PoseStamped ps;
    pose.header.stamp = ros::Time(0); // last transformation available
    tf_->transformPose(base_frame_, pose, ps);
    *x = ps.pose.position.x;
    *y = ps.pose.position.y;
    *theta = tf::getYaw(ps.pose.orientation);
    // std::cout << base_frame_ << " x: "<< *x << " y: "<< *y << " ,th: " << *theta << std::endl;
  }

  void BotsLocalPlanner::reconfigureCB(BotsLocalPlannerPlannerConfig &config, uint32_t level)
  {
    // target
    position_window = config.position_window;
    orientation_window = config.orientation_window;
    // goal tolerance
    position_precision = config.position_precision;
    orientation_precision = config.orientation_precision;
    // linear
    max_linear_vel_ = config.max_linear_vel;
    min_linear_vel = config.min_linear_vel;
    max_incr_vel_ = config.max_incr_vel;
    // angular
    max_angle_vel_ = config.max_angle_vel;
    min_angle_vel_ = config.min_angle_vel;
    min_incr_vel_ = config.min_incr_vel;
    // pid controller params
    kp_linear_ = config.kp_linear;
    ki_linear_ = config.ki_linear;
    kd_linear_ = config.kd_linear;

    kp_angle_ = config.kp_angle;
    ki_angle_ = config.ki_angle;
    kd_angle_ = config.kd_angle;
  }
};