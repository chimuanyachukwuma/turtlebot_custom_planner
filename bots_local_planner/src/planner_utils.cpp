#include "bots_local_planner/planner_utils.h"

LocalPlannerUtils::LocalPlannerUtils(double dt,
                                     double kp_linear,
                                     double ki_linear,
                                     double kd_linear,
                                     double kp_angle,
                                     double ki_angle,
                                     double kd_angle,
                                     double min_incr_vel,
                                     double max_incr_vel,
                                     double min_linear_vel,
                                     double max_linear_vel,
                                     double min_angle_vel,
                                     double max_angle_vel)
{
    double dt_ = dt;
    double kp_linear_ = kp_linear;
    double ki_linear_ = ki_linear;
    double kd_linear_ = kd_linear;
    double kp_angle_ = kp_angle;
    double ki_angle_ = ki_angle;
    double kd_angle_ = kd_angle;
    double min_incr_vel_ = min_incr_vel;
    double max_incr_vel_ = max_incr_vel;
    double min_linear_vel_ = min_linear_vel;
    double max_linear_vel_ = max_linear_vel;
    double min_angle_vel_ = min_angle_vel;
    double max_angle_vel_ = max_angle_vel;
}

LocalPlannerUtils::~LocalPlannerUtils() {}

void LocalPlannerUtils::getOdomPose(tf::Stamped<tf::Pose> goal_pose, double &position_x,
                                    double &position_y, double &yaw)
{
    position_x = goal_pose.getOrigin().x();
    position_y = goal_pose.getOrigin().y();
    yaw = tf::getYaw(goal_pose.getRotation());
}

std::vector<double> LocalPlannerUtils::getCurrentRobotPose(costmap_2d::Costmap2DROS *costmap_ros)
{
    double curr_x, curr_y, curr_yaw;
    geometry_msgs::PoseStamped current_pose;

    costmap_ros->getRobotPose(current_pose);
    curr_x = current_pose.pose.position.x;
    curr_x = current_pose.pose.position.y;
    curr_yaw = tf::getYaw(current_pose.pose.orientation);

    return {curr_x, curr_y, curr_yaw};
}

double LocalPlannerUtils::computePIDAngularVelocity(nav_msgs::Odometry &base_odometry,
                                                    double goal_yaw,
                                                    double curr_yaw,
                                                    double angle_error_,
                                                    double integral_angle_)
{
    double orientation_error = goal_yaw - curr_yaw;

    if (orientation_error > M_PI)
        orientation_error -= (2 * M_PI);
    else
        orientation_error += (2 * M_PI);

    double target_angle_vel = (orientation_error) / dt_;

    if (fabs(target_angle_vel) > max_angle_vel_)
    {
        target_angle_vel = copysign(max_angle_vel_, target_angle_vel);
    }

    double angle_vel = base_odometry.twist.twist.angular.z;
    double angle_error = target_angle_vel - angle_vel;
    integral_angle_ += angle_error * dt_;
    double derivative_angle = (angle_error - angle_error_) / dt_;
    double incr_ang = kp_angle_ * angle_error + ki_angle_ * integral_angle_ +
                      kd_angle_ * derivative_angle;
    angle_error_ = angle_error;

    if (fabs(incr_ang) > min_incr_vel_)
        incr_ang = copysign(min_incr_vel_, incr_ang);

    double theta_vel = copysign(angle_vel + incr_ang, target_angle_vel);
    if (fabs(theta_vel) > max_angle_vel_)
        theta_vel = copysign(max_angle_vel_, theta_vel);
    if (fabs(theta_vel) < min_angle_vel_)
        theta_vel = copysign(min_angle_vel_, theta_vel);

    return theta_vel;
}

double LocalPlannerUtils::computePIDLinearVelocity(nav_msgs::Odometry &base_odometry,
                                                   double goal_x,
                                                   double goal_y,
                                                   double linear_error_,
                                                   double integral_linear_)
{
    double current_vel = hypot(base_odometry.twist.twist.linear.y, base_odometry.twist.twist.linear.x);

    double target_vel = hypot(goal_x, goal_y) / dt_; //

    if (fabs(target_vel) > max_linear_vel_)
    {
        target_vel = copysign(max_linear_vel_, target_vel);
    }

    double error_vel = target_vel - current_vel;

    integral_linear_ += error_vel * dt_;
    double derivative_lin = (error_vel - linear_error_) / dt_;
    double incr_lin = kp_linear_ * error_vel + ki_linear_ * integral_linear_ + kd_linear_ * derivative_lin;
    linear_error_ = error_vel;

    if (fabs(incr_lin) > max_incr_vel_)
        incr_lin = copysign(max_incr_vel_, incr_lin);

    double vel_x = current_vel + incr_lin;

    if (fabs(vel_x) > max_linear_vel_)
        vel_x = copysign(max_linear_vel_, vel_x);
    else if (fabs(vel_x) < min_linear_vel_)
        vel_x = copysign(min_linear_vel_, vel_x);

    return vel_x;
}

bool LocalPlannerUtils::checkPathHeadingAchieved(double goal_yaw, double curr_yaw, double orientation_precision)
{
    if (fabs(goal_yaw - curr_yaw) < orientation_precision)
        return true;
    return false;
}

double LocalPlannerUtils::getGoalDistance(double curr_x, double curr_y, double goal_x, double goal_y)
{
    return hypot((goal_x - curr_x), (goal_y - curr_y));
}

// void atFinalPosition()
